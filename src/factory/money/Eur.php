<?php

namespace src\factory\money;

use src\factory\InterfaceCurrency;
use Exception;

class Eur implements InterfaceCurrency {

    public $cache;
    public $dateBase;
    public $http;

    public function getCache() : string {

        $this->cache = '72,09 cache';
        if ($this->cache) {
            return $this->cache;
        }return '';
    }

    public function getDB() : string {

        $this->dateBase = '72,09 dateBase';
        if ($this->dateBase) {
            return $this->dateBase;
        }return '';
    }

    public function getHttp() : string {

        $this->http = '72,09 http';
        if ($this->http) {
            return $this->http;
        }return '';
    }

    public function insertDate() {

        //Тут запись куда нужно;
    }

    public function getCurrency() {


        if ($this->getCache()):
            return $this->cache;
        elseif ($this->getDB()):
            return $this->dateBase;
        elseif ($this->getHttp()):
            $this->insertDate();
            return $this->http;
        else:

            try {
                throw new Exception("Какое-нибудь сообщение об ошибке", 30);
            } catch (Exception $e) {
                echo "Не могу получить данные о валюте: " . $e->getCode();
            }

        endif;
    }

}
