<?php

namespace src\factory;

use src\factory\money\Usd;
use src\factory\money\Eur;
use Exception;

class Factory {

    public static function bild($currency) {

        switch ($currency) {
            case 'USD':
                return new Usd();
                break;
            case 'EUR':
                return new Eur();
                break;
            default : exit("Нет такой валюты"); //Тут может быть исключение или еще что-то!
        }
    }

}
