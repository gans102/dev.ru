<?php

namespace src\factory;

interface InterfaceCurrency
{
    
    public function getCache() : string;
    
    public function getDB() : string;
    
    public function getHttp() : string;
    
    public function getCurrency();
    
    
}
